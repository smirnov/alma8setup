#!/bin/sh
#
# Ganglia installation script for AlmaLinux 8
#
# If it is not yet done - install EPEL repository:
#   dnf repolist all
#   dnf config-manager --set-enabled powertools
#   dnf install epel-release
#
dnf -y install ganglia-gmond
#/bin/vi /etc/ganglia/gmond.conf:
#             name = "Lxfarm"
#             owner = "MEPhI"
#             latlong = "N55.651 E37.663"
#             url = "http://www.lxfarm.mephi.ru"
#             location = "rackNo,lowest_unitNo*10,0"
systemctl enable gmond.service
#systemctl start gmond.service
systemctl status gmond.service
##
