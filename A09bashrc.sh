#!/bin/sh
#
file=${HOME}/.bashrc
if [ ! -f ${file} ]; then
  touch ${file}
fi

echo "">>${file}

echo "export HISTSIZE=10000">>${file}
echo "export HISTTIMEFORMAT='%d.%m.%y %H:%M '">>${file}
echo -en "export PS1=\x22">>${file}
echo -En "\[\033[1;31m\][\u@\h] \[\033[0m\]\[\033[0;35m\]\W # \[\033[0m\]">>${file}
echo -e  "\x22">>${file}
echo "">>${file}
echo "alias rm='/bin/rm -i'">>${file}
echo "alias cp='/bin/cp -i'">>${file}
echo "alias mv='/bin/mv -i'">>${file}
##
