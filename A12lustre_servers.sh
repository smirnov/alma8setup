#!/bin/sh
#
lustre_site=https://downloads.whamcloud.com/public
kernel_major=4.18.0
kernel_minor=553.27.1
lustre_vers=2.15.6
rhel_release=el8.10

e2fsprogs_vers=1.47.1
#
r=`/bin/uname -r`
kernel=${kernel_major}-${kernel_minor}.el8_10.x86_64
#
if [ $r != $kernel ]; then
  echo "Kernel version mismatch! Aborting ... " $r " " $kernel
  exit 1
fi
#
file1=kernel-${kernel_major}-${kernel_minor}.el8_lustre.x86_64.rpm
if [ ! -e ${file1} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file1}
  if [ $? != "0" ]; then exit 1; fi
fi
file2=kernel-core-${kernel_major}-${kernel_minor}.el8_lustre.x86_64.rpm
if [ ! -e ${file2} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file2}
  if [ $? != "0" ]; then exit 1; fi
fi
file3=kernel-modules-${kernel_major}-${kernel_minor}.el8_lustre.x86_64.rpm
if [ ! -e ${file3} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file3}
  if [ $? != "0" ]; then exit 1; fi
fi
file4=lustre-${lustre_vers}-1.el8.x86_64.rpm
if [ ! -e ${file4} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file4}
  if [ $? != "0" ]; then exit 1; fi
fi
file5=kmod-lustre-${lustre_vers}-1.el8.x86_64.rpm
if [ ! -e ${file5} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file5}
  if [ $? != "0" ]; then exit 1; fi
fi
file6=kmod-lustre-osd-ldiskfs-${lustre_vers}-1.el8.x86_64.rpm
if [ ! -e ${file6} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file6}
  if [ $? != "0" ]; then exit 1; fi
fi
file7=lustre-osd-ldiskfs-mount-${lustre_vers}-1.el8.x86_64.rpm
if [ ! -e ${file7} ]; then
  wget --no-check-certificate ${lustre_site}/lustre/lustre-${lustre_vers}/${rhel_release}/server/RPMS/x86_64/${file7}
  if [ $? != "0" ]; then exit 1; fi
fi
file8=e2fsprogs-${e2fsprogs_vers}-wc2.el8.x86_64.rpm
if [ ! -e ${file8} ]; then
  wget --no-check-certificate ${lustre_site}/e2fsprogs/${e2fsprogs_vers}.wc2/el8/RPMS/x86_64/${file8}
  if [ $? != "0" ]; then exit 1; fi
fi
file9=e2fsprogs-libs-${e2fsprogs_vers}-wc2.el8.x86_64.rpm
if [ ! -e ${file9} ]; then
  wget --no-check-certificate ${lustre_site}/e2fsprogs/${e2fsprogs_vers}.wc2/el8/RPMS/x86_64/${file9}
  if [ $? != "0" ]; then exit 1; fi
fi
file10=libcom_err-${e2fsprogs_vers}-wc2.el8.x86_64.rpm
if [ ! -e ${file10} ]; then
  wget --no-check-certificate ${lustre_site}/e2fsprogs/${e2fsprogs_vers}.wc2/el8/RPMS/x86_64/${file10}
  if [ $? != "0" ]; then exit 1; fi
fi
file11=libcom_err-devel-${e2fsprogs_vers}-wc2.el8.x86_64.rpm
if [ ! -e ${file11} ]; then
  wget --no-check-certificate ${lustre_site}/e2fsprogs/${e2fsprogs_vers}.wc2/el8/RPMS/x86_64/${file11}
  if [ $? != "0" ]; then exit 1; fi
fi
file12=libss-${e2fsprogs_vers}-wc2.el8.x86_64.rpm
if [ ! -e ${file12} ]; then
  wget --no-check-certificate ${lustre_site}/e2fsprogs/${e2fsprogs_vers}.wc2/el8/RPMS/x86_64/${file12}
  if [ $? != "0" ]; then exit 1; fi
fi
#
#
dnf install ${file8} ${file9} ${file10} ${file11} ${file12}
#
dnf --nogpgcheck --disableexcludes=main install ${file1} ${file2} ${file3} ${file4} ${file5} ${file6} ${file7}
#
# echo "options lnet networks=tcp0(eth1)" > /etc/modprobe.d/lustre.conf
# send dsa public key from MDS to OSS
#
# create file systems
# MGS:
# mkfs.lustre --fsname=lustre2 --mgs /dev/md124
# MDS:
# mkfs.lustre --fsname=lustre2 --mgsnode=192.168.77.40@tcp0 --mdt --index=0 /dev/sda
# OSS:
# mkfs.lustre --fsname=temp --mgsnode=192.168.77.118@tcp0 --ost --index=2 /dev/sda
#
# mkdir -p /mnt/ost2
# vi /etc/fstab
# mount -t lustre /dev/sda /mnt/ost2
#
# configure network
# lnetctl lnet configure --all
#
