#!/bin/sh
#
# Services fine-tuning (mainly disabling) for Alma8
services="firewalld.service"
for j in $services
do
    /usr/bin/systemctl disable $j
    /usr/bin/systemctl stop $j
    /usr/bin/systemctl mask $j
    /usr/bin/systemctl status $j
done
#
#if [ -e /etc/xdg/autostart/cern-alerterauto.desktop ]; then
#  echo "Removing cern-alerter"
#  yum -y remove cern-alerter
#fi
##
