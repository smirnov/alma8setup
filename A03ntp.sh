#!/bin/sh
#
# from linuxconfig.org
#
# Configure NTP server
# --------------------
#
# dnf install chrony
# systemctl enable chronyd
# vi /etc/chrony.conf:
#        allow 192.168.77.0/24
# systemctl restart chronyd
#* firewall-cmd --permanent --add-service=ntp
#* firewall-cmd --reload
# chronyc clients
#
# Configure NTP client
# --------------------
#
# dnf install chrony
# systemctl enable chronyd
# vi /etc/chrony.conf
#        server 192.168.77.40
# systemctl restart chronyd
# chronyc sources
#
# next is obsolete:
##if [ ! -f /usr/lib/systemd/system/ntpd.service ]; then
##    yum -y install ntp
##fi
##if [ -f /etc/ntp.conf ]; then
##    mv /etc/ntp.conf /etc/ntp.conf.save
##fi
#
##
