#!/bin/sh
#
sysctl_file1=/etc/sysctl.d/97-ip_forwarding.conf
if [ -f ${sysctl_file1} ]; then
  cp ${sysctl_file1} ${sysctl_file1}.save
fi
tmp_file1=./ip_forwarding.conf.tmp
/bin/rm -f ${tmp_file1}
#
echo "net.ipv4.ip_forward=1">>${tmp_file1}
/bin/mv -u ${tmp_file1} ${sysctl_file1}
/sbin/sysctl --load=${sysctl_file1}
#
sysctl_file2=/etc/sysctl.d/98-tcp_buffer.conf
if [ -f ${sysctl_file2} ]; then
  cp ${sysctl_file2} ${sysctl_file2}.save
fi
#
tmp_file2=./tcp_buffer.conf.tmp
/bin/rm -f ${tmp_file2}
#
echo "#">>${tmp_file2}
echo "# increase TCP max buffer size">>${tmp_file2}
echo "# taken from monalisa.cern.ch/FDT/documentation_syssettings.html">>${tmp_file2}
echo "net.core.rmem_max = 10485760">>${tmp_file2}
echo "net.core.wmem_max = 10485760">>${tmp_file2}
echo "net.ipv4.tcp_rmem = 4096 87380 10485760">>${tmp_file2}
echo "net.ipv4.tcp_wmem = 4096 65536 10485760">>${tmp_file2}
echo "net.core.netdev_max_backlog = 250000">>${tmp_file2}
echo "net.ipv4.tcp_no_metrics_save = 1">>${tmp_file2}
echo "net.ipv4.tcp_moderate_rcvbuf = 1">>${tmp_file2}
echo "# and from wiki.archlinux.org/title/Sysctl_(Русский)">>${tmp_file2}
echo "net.core.somaxconn = 4096">>${tmp_file2}
mv -u ${tmp_file2} ${sysctl_file2}
/sbin/sysctl --load=${sysctl_file2}
##
