#!/bin/sh
#
# Check that IPTABLES are disabled
my_systemctl=/usr/bin/systemctl
# $my_systemctl stop    iptables
# $my_systemctl disable iptables
# $my_systemctl mask    iptables
#
# Check that FIREWALLD is enabled and started
# $my_systemctl unmask firewalld
# $my_systemctl enable firewalld
# $my_systemctl start  firewalld
my_firewall-cmd=/usr/bin/firewall-cmd
# $my_firewall-cmd --state
#
# Next comes the configuration example for mgs.lxfarm.mephi.ru
$my_firewall-cmd --zone=trusted --add-interface=enp2s0 --permanent
$my_firewall-cmd --zone=public  --add-interface=eno1   --permanent
#
$my_firewall-cmd --zone=public --add-service=ntp --permanent
$my_firewall-cmd --zone=public --remove-service=ssh           --permanent
$my_firewall-cmd --zone=public --remove-service=cockpit       --permanent
$my_firewall-cmd --zone=public --remove-service=dhcpv6-client --permanent
$my_firewall-cmd --zone=public --add-rich-rule='rule family="ipv4" source address="85.143.112.0/22" service name="ssh"     accept' --permanent
$my_firewall-cmd --zone=public --add-rich-rule='rule family="ipv4" source address="85.143.112.0/22" service name="cockpit" accept' --permanent
#
$my_firewall-cmd --reload
#
# Rules for the IP-forwarding
#$my_firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o eno1 -j MASQUERADE
#$my_firewall-cmd --direct --add-rule ipv4 filter FORWARD  0 -i enp2s0 -o eno1 -j ACCEPT
#$my_firewall-cmd --direct --add-rule ipv4 filter FORWARD  0 -i eno1 -o enp2s0 -m state --state RELATED,ESTABLISHED -j ACCEPT
#$my_firewall-cmd --runtime-to-permanent
##
