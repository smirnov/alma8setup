#!/bin/sh
#
if [ -f /etc/selinux/config ]; then
    mv /etc/selinux/config /etc/selinux/config.save
fi

file=${HOME}/selinux_config.tmp
rm -f ${file}
touch ${file}

echo "# This file controls the state of SELinux on the system.">>${file}
echo "# SELINUX= can take one of these three values:">>${file}
echo "#     enforsing - SELinux security policy is enforsed.">>${file}
echo "#     permissive - SELinux prints warnings instead of enforcing.">>${file}
echo "#     disabled - No SELinux policy is loaded.">>${file}
echo "SELINUX=disabled">>${file}
echo "# SELINUXTYPE= can take one of these two values:">>${file}
echo "#     targeted - Targeted processes are protected,">>${file}
echo "#     mls - Multi Level Security protection.">>${file}
echo "SELINUXTYPE=targeted">>${file}

mv ${file} /etc/selinux/config
setenforce 0
echo "sestatus:"
echo "========="
sestatus
##
